<?php

use Illuminate\Database\Seeder;

class TracksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        // Tracks 
        for($i = 0; $i < 150; $i++) {
        	DB::table('tracks')->insert([
        	    'start_time' => $faker->dateTimeThisDecade('now')->format('U'),
        	    'total_time' => $faker->numberBetween(100,13000),
        	    'description' => $faker->realText(100,1),
        	    'project_id' => $faker->numberBetween(1,40)
        	]);
        }

    }
}
