<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        // Projects Table
        for($i = 0; $i < 40; $i++) {
        	DB::table('projects')->insert([
        	    'title' => $faker->domainName,
        	    'description' => $faker->realText(70,1),
        	    'client_id' => $faker->numberBetween(1,8)
        	]);
        }
        
    }

}
