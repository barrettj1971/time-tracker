<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // Clients Table
        for ($i = 0; $i < 8; $i++) {
            DB::table('clients')->insert([ //,
                'name' => $faker->company,
                'contact_name' => $faker->name,
                'contact_phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'standard_hourly_rate' => $faker->numberBetween(40,140)
            ]);
        }
    }
}
