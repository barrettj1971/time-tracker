<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
            $table->integer('start_time');
            $table->integer('end_time')->default(0);
            $table->integer('total_time')->default(0);
            $table->text('description')->nullable();
            $table->integer('project_id')->unsigned();
            $table->tinyInteger('is_untracked')->default(0);
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tracks');
    }
}
