<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function client() {
    	return $this->belongsTo('App\Client');
    }
    
    public function tracks() {
    	return $this->hasMany('App\Track');
    }
    
    public function untracks() {
    	return $this->hasMany('App\Untrack');
    }
}
