<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Client;

class ClientsController extends Controller 
{

    public function testReports($id) {
        return Client::find($id);
    }

    public function clientsFetch()
    {
    	return Client::get();
    }

    public function singleClient($id) {
        $client = Client::find($id);
        $client->clientProjects = $client->projects;
        return $client;
    }
    
    public function createClient(Request $request) {
        $client = new Client;
        $client->name=$request->input('name');
        if($client->save()) {
            return $client;
        } else {
            return false;
        }
    }
    
    public function updateClient(Request $request) {
        $client = Client::find($request->input('id'));
        $client->name = $request->input('name');
        $client->contact_name = $request->input('contact_name');
        $client->contact_phone = $request->input('contact_phone');
        $client->address = $request->input('address');
        $client->standard_hourly_rate = $request->input('standard_hourly_rate');
        
        $client->save();
        return $client;
    }
}
