<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Project;

class ProjectsController extends Controller
{
    public function createProject(Request $request) {
        $project = new Project;
        $project->title=$request->input('title');
        $project->client_id=$request->input('client_id');
        $project->description = $request->input('description');
        if($project->save()) {
            return $project;
        } else {
            return false;
        }
    }
    
    public function updateProject(Request $request) {
        $project = Project::find($request->input('id'));
        $project->title = $request->input('title');
        $project->description = $request->input('description');
        
        $project->save();
        return $project;
    }

    public function singleProject($id) {
        $project = Project::find($id);
        // get tracks and perform modifications to time, etc.
        $projectTracks = $project->tracks;
        foreach($projectTracks as $pt) {
            // #seconds between start time and end time.
            $seconds = $pt->total_time;
            // convert time in seconds to usable format.
            $hours = floor($seconds / 3600);
            $mins = floor(($seconds - ($hours*3600)) / 60);
            // leading zeros
            /*if($hours<10) {
                $hours = '0' . $hours;
            }*/
            if($mins<10) {
                $mins = '0' . $mins;
            }

            $timestring = $hours . ':' . $mins;
            
            $pt->total = $timestring;
            $pt->start_time = date('m/d/Y g:i a',$pt->start_time);
            $pt->end_time = date('m/d/Y g:i a',$pt->end_time);

            
        }
        $project->projectTracks = $projectTracks;
        return $project;
    }
}
