<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Track;

class TracksController extends Controller
{
    public function createTrack(Request $request) {
        $track = new Track;
        // calculate total time in seconds
        $total = $request->input('hours')*3600 + $request->input('minutes')*60;
        
        $track->description = $request->input('description');
        $track->project_id = $request->input('project_id');
        $track->start_time = strtotime($request->input('start_time'));
        $track->total_time = $total;
        $track->save();
        return $track;
    }

    /**
     * Start Tracking Time
     */
    public function startTrack(Request $request) {
        $id = $request->input('id');
        if($id==0) {
            $track = new Track;
        } else {
            // do something else here.
        }
        $track->start_time = time();
        $track->project_id = $request->input('project_id');
        $track->save();
        return $track;

    }

    /**
     * Stop tracking time 
     * 
     */
    public function stopTrack(Request $request) {
        $track = Track::find($request->input('id'));
        $track->end_time = time();
        $track->total_time = $track->end_time - $track->start_time;
        $track->save();
        // #seconds between start time and end time.
            $seconds = $track->total_time;
            // convert time in seconds to usable format.
            $hours = floor($seconds / 3600);
            $mins = floor(($seconds - ($hours*3600)) / 60);
            // leading zeros
            if($hours<10) {
                $hours = '0' . $hours;
            }
            if($mins<10) {
                $mins = '0' . $mins;
            }

            $timestring = $hours . ':' . $mins;
            
            $track->total = $timestring;
            $track->start_time = date('m/d/Y g:i a',$track->start_time);
            $track->end_time = date('m/d/Y g:i a',$track->end_time);
        return $track;
    }

    public function updateTrackDescription(Request $request) {
        $track = Track::find($request->input('id'));
        $track->description = $request->input('description');
        $track->save();
        return $track;
    }

    public function singleTrack($id) {
        $track = Track::find($id);
        return $track;
    }

    public function deleteTrack($id) {
        Track::destroy($id);

    }
}
