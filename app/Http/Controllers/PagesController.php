<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index() {
    	$data['active'] = 'home';
    	return view('pages.home', $data);
    }
    
    public function reports() {
    	$data['active'] = 'reports';
    	return view('pages.reports', $data);
    }
}
