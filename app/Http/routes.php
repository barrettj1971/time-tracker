<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/** 
*	Routes for testing
*/
Route::get('/test/reports/{id}', 'ClientsController@testReports');
Route::get('/test/faker', function() {
	$faker = Faker\Factory::create();
	echo '<pre>'; var_dump($faker->dateTimeThisDecade('now')); echo '</pre>';
	echo $faker->dateTimeThisDecade('now')->format('U');
});

/**
 * 
 */
Route::get('/','PagesController@index');
Route::get('/reports','PagesController@reports');
// client routes
Route::get('clients/fetch','ClientsController@clientsFetch');
Route::get('client/fetch/{id}', 'ClientsController@singleClient');
Route::post('client/create', 'ClientsController@createClient');
Route::post('client/update', 'ClientsController@updateClient');
//project routes
Route::post('project/create', 'ProjectsController@createProject');
Route::post('project/update', 'ProjectsController@updateProject');
Route::get('project/fetch/{id}', 'ProjectsController@singleProject');
// track routes
Route::post('track/start', 'TracksController@startTrack');
Route::post('track/stop', 'TracksController@stopTrack');
Route::post('track/update-description', 'TracksController@updateTrackDescription');
Route::get('track/fetch/{id}', 'TracksController@singleTrack');
Route::post('track/create', 'TracksController@createTrack');
Route::delete('track/delete/{id}','TracksController@deleteTrack');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
