//var Vue = require('vue');
//Vue.use(require('vue-resource'));

vm = new Vue({
  el:'body', 

  data: {
    // body class -- used with modals
    bodyClass: {
        modalOpen:false
    },
    // global
    clients: [],
    panel : 'welcome',
    formPanel: '',
    // clients
    addClientFormVisible : false,
    newClient : '',
    currentClient: [],
    showClientEdit: false,
    // project
    showProjectCreate: false,
    showProjectEdit: false,
    newProjectTitle: '',
    newProjectDescription: '',
    currentProject: false,
    //**************
    // tracking 
    //**************
      // show/hide tracker 
    showTracker: false,
      // currently selected track instance
    currentTrack: false,
      // show/hide form to update the description on a current track
      // while track is active
    showTrackDescriptionForm: false,
      // modal window with form to edit an existing track
    showTrackModal:false,
      // show/hide form to create new untracked time
    showNewUntrackedForm:false,
      // object to hopefully hold my new track data
    newTrack: {},
      // counter
      counterHours: '00',
      counterMinutes: '00',
      counterSeconds: '00',
      intervalCounter: false,
  },

  // Look in to computed properties for handling clients list??
  
  created() {
    this.fetchClientsList();
  }, 

  methods: {
    /* get all clients */
    fetchClientsList() {
      $.getJSON('clients/fetch', function(data) {
          this.clients = data;
      }.bind(this));
    },

    clientDetail(clientID) {
      var vm = this;
      $.getJSON('client/fetch/' + clientID, function(data) {
        vm.currentClient = data;
        vm.showClientEdit = false;
        vm.panel = 'client';
      });
    },

    addClient() {
      var vm = this;
      $.ajax({
        url: '/client/create',
        type: 'post',
        data: {name: vm.newClient},
        success: function(data) {
          vm.newClient = '';
          vm.clients.push(data);
          vm.currentClient = data;
          vm.addClientFormVisible = false;
          vm.showClientEdit = true;
          vm.panel = 'client';
        },
        error: function(e) {
          console.log(e);
          document.write(e.responseText);
          alert('failed');
        }
      });
    },

    clientEditSubmit() {
      var vm = this;
      
      $.ajax({
          url: 'client/update',
          type: "post",
          data: {
            id: vm.currentClient.id,
            name: vm.currentClient.name,
            contact_name: vm.currentClient.contact_name,
            contact_phone: vm.currentClient.contact_phone,
            address: vm.currentClient.address,
            standard_hourly_rate: vm.currentClient.standard_hourly_rate
          },
          success: function(data){
            vm.currentClient = vm.clientDetail(vm.currentClient.id);
          },
          error: function() {
            alert('failed');
          }
        }); 
    },

// PROJECTS
    addProject() {
      var vm=this;
      $.ajax({
        url:'/project/create',
        type: 'post',
        data: {title:vm.newProjectTitle,client_id:vm.currentClient.id,description:vm.newProjectDescription},
        success: function(data) {
          vm.showProjectCreate = false;
          vm.newProjectTitle = '';
          vm.newProjectDescription = '';
          //vm.currentClient.clientProjects.push(data);
          vm.currentProject = data;
          vm.panel='project';
        }
      });
    },

    projectDetail(projectID) {
      var vm = this;
      $.getJSON('project/fetch/' + projectID, function(data) {
        vm.currentProject = data;
        vm.showProjectEdit = false;
        vm.panel = 'project';
      });
    },
    setCurrentProject(projectId) {
        $.getJSON('project/fetch/' + projectId, function () {
            vm.currentProject = data;
        });
    },
    projectEditSubmit() {
      var vm = this;
      
      $.ajax({
          url: 'project/update',
          type: "post",
          data: {
            id: vm.currentProject.id,
            title: vm.currentProject.title,
            description: vm.currentProject.description
          },
          success: function(data){
            //vm.projectDetail(data.id);
            vm.showProjectEdit = false;
          },
          error: function(e) {
            alert('failed');
          }
        });
    },

    // tracks
    stopTrack() {
      var vm = this;
      $.ajax({
        url:'track/stop',
        type: 'post',
        data: {
          id: vm.currentTrack.id
        },
        success: function(data) {
          vm.showTracker = false;
          // refresh the project
          vm.projectDetail(data.project_id);
          
          vm.currentTrack = false;
          vm.counterHours = '00';
          vm.counterMinutes = '00';
          vm.counterSeconds = '00';
          clearInterval(vm.intervalCounter);
        },
        error: function() {
          alert('failed to stop');
        }
      });
    },
    startTrack(trackId,projectId) {
      var vm = this;
      if(!projectId) {
        projectId = vm.currentProject.id;
      } else {
        vm.setCurrentProject(projectId);
      }
      vm.showTracker = true;

      $.ajax({
          url: 'track/start',
          type: "post",
          data: {
            id:trackId,
            project_id:projectId
          },
          success: function(data){
            vm.currentTrack = data;
            vm.showTracker = true;
              vm.intervalCounter = setInterval(function() {
                vm.counterSeconds++;
                // if seconds = 60, increment minutes and reset
                if(vm.counterSeconds==60) {
                  vm.counterSeconds = 0;
                  vm.counterMinutes++;
                // if minutes = 60, increment hours and reset
                  if(vm.counterMinutes==60) {
                    vm.counterMinutes=0;
                    vm.counterHours++;
                  }
                }
                // leading zero for seconds
                if(vm.counterSeconds.toString().length<2) {
                  vm.counterSeconds = '0' + vm.counterSeconds;
                }
                // leading zero for minutes
                if(vm.counterMinutes.toString().length<2) {
                  vm.counterMinutes = '0' + vm.counterMinutes;
                }
              },1000);
          },
          error: function() {
            alert('failed');
          }
        });
      

    },

    updateTrackDescription() {
      var vm = this;
      $.ajax({
        url:'track/update-description',
        type: 'post',
        data: {
          description: vm.currentTrack.description,
          id:vm.currentTrack.id
        },
        success: function(data) {
          //console.log(data);
          vm.showTrackDescriptionForm = false;
        },
        error: function() {
          alert('update failed');
        }
      });
    },
    
    editTrack(trackId) {
      var vm = this;
      $.getJSON('/track/fetch/' + trackId,function(data) {
        vm.currentTrack = data;
        vm.showTrackModal = true;
      });
    },
    
    trackEditSubmit() {
        console.log('track edit form submitted');
    },

  }
});

