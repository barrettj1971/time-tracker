## Time Tracker App

My personal time tracking app using laravel backend and single page vue app for front end.
This is intended to make it easier to track my work throughout the day and make sure that time is billed to the correct client and project.

### To do after moving to server: ###

* 	create database
* 	create and edit .env
* 	npm install
* 	bower install
* 	php artisan migrate

Can also do ***php artisan db:seed*** to insert phony data using Faker