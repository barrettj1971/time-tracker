<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title v-if="showTracker">@{{counterHours}}:@{{counterMinutes}}:@{{counterSeconds}} -- {{config('site.title')}}</title>
  <title v-if="!showTracker">{{ config('site.title') }}</title>
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <link rel="stylesheet" type="text/css" href="/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="/css/app.css">

  @yield('after_header')

  <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body :class="bodyClass">

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Time Tracker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class=""><a href="/">Home</a></li>
            <!--<li class=""><a href="/reports">Reports</a></li>
            <li :class="panel=='welcome' ? 'active' : 'not-active'"><a href="/alt">Alt/Dev</a></li>-->
            <li><a @click="showTrackModal=true, bodyClass.modalOpen=true">Test Modal</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


      @yield('content')

<!-- modals -->
@include('partials.overlays')

  <footer class="footer">

  </footer>
	@yield('footer')

	<script type="text/javascript" src="js/vendor.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  

	<script type="text/javascript">
		$.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
	</script>

	@yield('after_footer')
  

</body>
</html>