<div class="overlay" id="trackModal" v-if="showTrackModal">
	<div class="overlayModal">
      <form @submit.prevent="trackEditSubmit">
        <label>Description</label>
        <p>this form doesn't work because i don't care about it yet</p>
        <input type="text" v-model="currentTrack.description" class="form-control">
      </form>
		  <a @click="showTrackModal=false">Close</a>
	</div>
</div>

<div class="overlay" id="newUntracked" v-if="showNewUntrackedForm">
  <div class="overlayModal">
    <form @submit.prevent="addTrack">
      <label>Description</label>
      <textarea v-model="newTrack.description" class="form-control"></textarea>
      <label>Date</label>
      <input type="date" class="form-control" v-model="newTrack.start_time">
      <label>Time</label>
      <input type="text" v-model="newTrack.hours" class="form-control" placeholder="hours"><input type="text" v-model="newTrack.minutes" class="form-control" placeholder="minutes">
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
      <a @click="showNewUntrackedForm=false">Close</a>
  </div>
</div>
