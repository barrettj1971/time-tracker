<!-- project panel -->
<div class="col-md-9"> 
	<div class="well" v-if="panel=='welcome'">
		<h3>Time Tracker App</h3>
		<p>Basically this gives you the ability to create and manage clients with multiple projects per client and then track time worked to specific projects. The main feature that I was interested in implementing is the tracker itself. Because I'm using vue, I'm able to keep the tracking clock open and uninterrupted even if I'm making changes elsewhere on the page (such as adding comments or modifying a project description).</p>
        <p>When time permits I plan to implement some type of reporting/export feature but I'm not sure yet what that looks like.</p>
        <hr>
        <h4>TODOS</h4>
        <ul>
        	<li>support for multiple users</li>
        	<li>restyle tracking clock and give ability to minimize to nav bar</li>
        	<li>Ability to run reports and export the results</li>
        	<li>find and remove any tracks &lt; 5 minutes (or 1 minute? or 15 minutes?)</li>
        </ul>
        <hr>
	</div>
</div>