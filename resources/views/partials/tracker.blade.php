<div id="tracker" class="overlayPanel" v-if="showTracker">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Tracking Time for @{{currentProject.title}} <i class="fa fa-refresh fa-spin"></i></h3>
				
				<span class="success" v-if="!currentTrack.description">What are you up to? ... </span>
				<div>
					<a @click="showTrackDescriptionForm = true" v-if="!showTrackDescriptionForm">Edit</a>
					<a @click="showTrackDescriptionForm = false" v-if="showTrackDescriptionForm">Cancel</a>
				</div>
				<form @submit.prevent="updateTrackDescription" v-if="showTrackDescriptionForm">
					<textarea class="form-control" placeholder="What are you working on? ... " rows="10" style="border-radius:3px 3px 0 0;" v-model="currentTrack.description"></textarea>
					<button type="submit" class="btn btn-success btn-block" style="border-radius:0 0 3px 3px;">Update</button>
				</form>
				<span v-if="!showTrackDescriptionForm">@{{currentTrack.description}}</span>
				
			</div>
			<div class="col-md-4">
				<h2>
					<span v-if="counterHours>0">@{{counterHours}}:</span>@{{counterMinutes}}:@{{counterSeconds}}
				</h2>
			</div>
			<div class="col-md-2">
				<a @click="stopTrack" class="btn btn-primary">Stop Tracking Time</a>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				
			</div>
		</div>
	</div>
	
</div>