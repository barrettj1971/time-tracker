	<!-- client panel -->
<div class="col-md-9">
	<div class="well" v-if="panel=='client'">

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<h1>@{{currentClient.name}}</h1>
					<a v-if="showClientEdit" @click="showClientEdit=false" class="title-edit">Cancel</a>
					<a v-if="!showClientEdit" @click="showClientEdit=true" class="title-edit">Edit</a>
					<form v-if="showClientEdit" @submit.prevent="clientEditSubmit" class="dropForm">
	                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="form-group">
						<label>Name</label>
						<input type="text" v-model="currentClient.name" class="form-control">
					</div>
					<div class="form-group">
						<label>Contact Name</label>
						<input type="text" name="contact_name" v-model="currentClient.contact_name" class="form-control">
					</div>
					<div class="form-group">
						<label>Contact Phone</label>
						<input type="text" name="contact_phone" v-model="currentClient.contact_phone" class="form-control">
					</div>
					<div class="form-group">
						<label>Address</label>
						<input type="text" name="address" v-model="currentClient.address" class="form-control">
					</div>
					<div class="form-group">
						<label>Hourly Rate</label>
						<input type="text" name="standard_hourly_rate" v-model="currentClient.standard_hourly_rate" class="form-control">
					</div>
					<div class="form-group text-right">
						<input type="submit" class="btn btn-info">
					</div>

					</form>
				</div>
				<div class="col-md-6 text-right">
					
				</div>
			</div>
			<div class="row">

				<div class="col-md-6">
					
					
					
						<div>Contact Name: @{{currentClient.contact_name}}</div>
						<div>Contact Phone: @{{currentClient.contact_phone}}</div>
						<div>Address: @{{currentClient.address}}</div>
						<div>Rate: @{{currentClient.standard_hourly_rate}}</div>
					
					
				</div>
			</div>
		</div>

		<hr>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<h3>Projects</h3>
				</div>
				<div class="col-md-6">
					<div id="addProject" class="text-right">
						<a v-if="!showProjectCreate" @click="showProjectCreate=true">Add a New Project</a>
						<a v-if="showProjectCreate" @click="showProjectCreate=false">Cancel</a>
					</div>
					
					<form v-if="showProjectCreate" @submit.prevent="addProject" class="dropForm">
						<input type="text" class="form-control" v-model="newProjectTitle" placeholder="Project Title">
						<textarea v-model="newProjectDescription" placeholder="Project Description" class="form-control"></textarea>
						<button type="submit" class="btn btn-info">Submit</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					
					<table class="table">
						<thead>
							<th>Project</th>
							<th>Description</th>
							<th></th>
						</thead>
						<tbody>
							<tr v-for="p in currentClient.clientProjects">
								<td><a @click="projectDetail(p.id)">@{{p.title}}</a></td>
								<td>@{{p.description}}</td>
								<td><a @click="startTrack(0,p.id)"><i class="fa fa-clock-o"></i></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>
</div>
	<!-- end client panel -->