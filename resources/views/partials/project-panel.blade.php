	<!-- project panel -->
		<div class="col-md-9"> 
			<div class="well" v-if="panel=='project'">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-12">
							<h1>@{{currentClient.name}}</h1>
							<hr>
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-6">
							<h3>Project: @{{currentProject.title}}</h3>
							<a @click="showProjectEdit=true" v-if="!showProjectEdit">Edit</a>
							<a @click="showProjectEdit=false" v-if="showProjectEdit">Cancel</a>
							<span v-if="showProjectEdit">
								<form @submit.prevent="projectEditSubmit" class="dropForm">
									<div class="form-group">
										<label>Title</label>
										<input type="text" v-model="currentProject.title" class="form-control">
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea v-model="currentProject.description" v-model="currentProject.description" class="form-control"></textarea>
									</div>
									<div class="form-group text-right">
										<button type="submit" class="btn btn-info">Submit</button>
									</div>
								</form>
							</span>
						</div>
						<div class="col-md-6 text-right">
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							
							<p>@{{currentProject.description}}</p>
							
							
						</div>
					</div>
				</div>
				
				
				
				
				<hr>
				<div class="col-md-6">
					<h3>Tracked Time</h3>
				</div>
				<div class="col-md-6 text-right">
					<div id="start-tracking">
						<button @click="showNewUntrackedForm=true" class="btn btn-info">Enter Untracked Time</button>
						<button @click="startTrack(0)" v-if="!showTracker" class="btn btn-primary">Start Tracking</button>
					</div>
				</div>

				<table class="table">
					<thead>
						<tr>
							<th>Description</th>
							<th>Started At</th>
							<th>Total</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="t in currentProject.projectTracks">
							<td>@{{t.description}}</td>
							<td>@{{t.start_time}}</td>
							<td>@{{t.total}}</td>
							<td><!--<i class="fa fa-angle-right" @click="startTrack(t.id)"></i>-->
								<a class="fa fa-edit success" @click="editTrack(t.id)"></a>
								<a class="fa fa-close" @click="deleteTrack($index,t)"></a>
							</td>
						</tr>
					</tbody>
				</table>

			</div>
		</div>