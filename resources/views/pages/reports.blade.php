@extends('master')

@section('content')


<div class="container-fluid">
	<div class="row">
	
		<div class="col-md-12" v-if="!reportSubmitted" >
			<div class="well clearfix">
									
					<div class=" col-md-12 sort-buttons">
						<button class="btn btn-default btn-sm" @click="reportSelectAll">All</button>
						<button class="btn btn-info btn-sm" @click="reportSelectNone">None</button>
							<span class="pull-right">@{{checkedClients | json}}</span>
					</div>

				<form @submit.prevent="submitReport">

					<!-- CLIENTS -->
					<div class="col-md-3 col-sm-6" v-for="client in clients">
						<label class="checkbox-inline">
						  <input type="checkbox" value="@{{client.id}}" v-model="checkedClients"> @{{client.name}}
						</label>
					</div>
					<div class="clearfix"></div>
					<!-- DATES -->
					<div class="col-md-3 col-sm-6 form-group">
						<label>Start Date</label>
						<input type="date" v-model="reportDateStart" class="form-control">
						<label>End Date</label>
						<input type="date" v-model="reportDateEnd" class="form-control">
					</div>
					<div class="col-md-12">
						<button type="submit" class="btn btn-success">Retrieve Report</button>
					</div>
				</form>
			</div>
		</div>

		<!-- report has been submitted -->
		<div class="col-md-3" v-if="reportSubmitted">
			<div class="well">
				search params w/option to print or save or something? 
			</div>
		</div>
		
		<div class="col-md-9" v-if="reportSubmitted">
			<div class="well">
				results of query.
			</div>
		</div>

	</div><!-- top level row -->
</div><!-- top level container -->

		




@stop