@extends('master')

@section('content')


<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="well">	
				<h3>Clients</h3>
					<button v-for="client in clients" @click="clientDetail(client.id)" :class="client.id==currentClient.id ? ' btn btn-success btn-block' : 'btn btn-default btn-block'">@{{client.name}} ></button>

				<div class="text-left">
					<a @click="addClientFormVisible = true" v-if="!addClientFormVisible">Add New</a>
					<a @click="addClientFormVisible = false" v-if="addClientFormVisible">Cancel</a>
				</div>

				<form @submit.prevent="addClient" v-if="addClientFormVisible" class="dropForm">
					<legend>New Client</legend>
					<div class="form-group">
						<input type="text" class="form-control" v-model="newClient">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-info btn-block">
					</div>
				</form>

			</div>
		</div>

		@include('partials.welcome-panel')

		@include('partials.client-panel')

		@include('partials.project-panel')

		@include('partials.tracker')

	</div><!-- top level row -->
</div><!-- top level container -->

		




@stop