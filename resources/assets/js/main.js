//browserify entry point

var Vue = require('vue');
Vue.use(require('vue-resource'));

new Vue({
  el:'html', 

  data: {
    // body class -- used with modals
    bodyClass: {
        modalOpen:false
    },
    // global
    clients: [],
    panel : 'welcome',
    formPanel: '',
    // clients
    addClientFormVisible : false,
    newClient : '',
    currentClient: [],
    showClientEdit: false,
    // project
    showProjectCreate: false,
    showProjectEdit: false,
    newProjectTitle: '',
    newProjectDescription: '',
    currentProject: false,
    //**************
    // tracking 
    //**************
      // show/hide tracker 
    showTracker: false,
      // currently selected track instance
    currentTrack: false,
      // show/hide form to update the description on a current track
      // while track is active
    showTrackDescriptionForm: false,
      // modal window with form to edit an existing track
    showTrackModal:false,
      // show/hide form to create new untracked time
    showNewUntrackedForm:false,
      // object to hopefully hold my new track data
    newTrack: {},
      // counter
      counterHours: '00',
      counterMinutes: '00',
      counterSeconds: '00',
      intervalCounter: false,
    // reports 
    reportSubmitted: false,
    checkedClients: [],
  },

  // Look in to computed properties for handling clients list??
  
  created() {
    this.fetchClientsList();
  }, 

  methods: {
    /* get all clients */
    fetchClientsList() {
    	var vm = this;
    	this.$http.get('clients/fetch',function(data) {
    		vm.clients = data;
    	});
    },

    clientDetail(clientID) {
      var vm = this;
      this.$http.get('client/fetch/' + clientID, function(data) {
        vm.currentClient = data;
        vm.showClientEdit = false;
        vm.panel = 'client';
      });
    },

    addClient() {
      var vm = this;
      var vdata = {
      	name: vm.newClient
      };
      this.$http.post('/client/create',vdata,function(data) {
      	vm.newClient = '';
          vm.clients.push(data);
          vm.currentClient = data;
          vm.addClientFormVisible = false;
          vm.showClientEdit = true;
          vm.panel = 'client';
      });

    },

    clientEditSubmit() {
      var vm = this;
      var vdata = {
      		id: vm.currentClient.id,
            name: vm.currentClient.name,
            contact_name: vm.currentClient.contact_name,
            contact_phone: vm.currentClient.contact_phone,
            address: vm.currentClient.address,
            standard_hourly_rate: vm.currentClient.standard_hourly_rate
      };
      
      this.$http.post('client/update',vdata,function(data) {
      	vm.currentClient = vm.clientDetail(vm.currentClient.id);
      });
    },

// PROJECTS
    addProject() {
      var vm=this;
      var vdata = {
      	title:vm.newProjectTitle,
      	client_id:vm.currentClient.id,
      	description:vm.newProjectDescription
      };
      this.$http.post('project/create',vdata,function(data) {
	  	  vm.showProjectCreate = false;
	      vm.newProjectTitle = '';
	      vm.newProjectDescription = '';
	      vm.currentProject = data;
	      vm.panel='project';
      });
    },

    projectDetail(projectID) {
      var vm = this;
      this.$http.get('project/fetch/' + projectID, function(data) {
        vm.currentProject = data;
        vm.showProjectEdit = false;
        vm.panel = 'project';
      });
    },
    setCurrentProject(projectId) {
        this.$http.get('project/fetch/' + projectId, function () {
            vm.currentProject = data;
        });
    },
    projectEditSubmit() {
      var vm = this;
      var vdata = {
            id: vm.currentProject.id,
            title: vm.currentProject.title,
            description: vm.currentProject.description
      };
      this.$http.post('project/update',vdata,function(data) {
      		vm.showProjectEdit = false;
      });
    },

    // tracks
    stopTrack() {
      var vm = this;
      var vdata = {
      		id: vm.currentTrack.id
      };
      this.$http.post('track/stop',vdata,function(data) {
      		vm.showTracker = false;
      		vm.projectDetail(data.project_id);
	        vm.currentTrack = false;
	        vm.counterHours = '00';
	        vm.counterMinutes = '00';
	        vm.counterSeconds = '00';
	        clearInterval(vm.intervalCounter);
      });
    },
    startTrack(trackId,projectId) {
      var vm = this;

      if(!projectId) {
        projectId = vm.currentProject.id;
      } else {
        vm.setCurrentProject(projectId);
      }

      var vdata = {
      		id: trackId,
      		project_id: projectId
      };

      this.$http.post('track/start',vdata,function(data) {
      	// there must be a better way to handle this?
            vm.currentTrack = data;
            vm.showTracker = true;
              vm.intervalCounter = setInterval(function() {
                vm.counterSeconds++;
                // if seconds = 60, increment minutes and reset
                if(vm.counterSeconds==60) {
                  vm.counterSeconds = 0;
                  vm.counterMinutes++;
                // if minutes = 60, increment hours and reset
                  if(vm.counterMinutes==60) {
                    vm.counterMinutes=0;
                    vm.counterHours++;
                  }
                }
                // leading zero for seconds
                if(vm.counterSeconds.toString().length<2) {
                  vm.counterSeconds = '0' + vm.counterSeconds;
                }
                // leading zero for minutes
                if(vm.counterMinutes.toString().length<2) {
                  vm.counterMinutes = '0' + vm.counterMinutes;
                }
              },1000);
      });
    },

    updateTrackDescription() {
      var vm = this;

      var vdata = {
          description: vm.currentTrack.description,
          id:vm.currentTrack.id
      };

      this.$http.post('track/update-description',vdata,function(data) {
      		vm.showTrackDescriptionForm = false;
      });
    },
    
    editTrack(trackId) {
      var vm = this;
      this.$http.get('/track/fetch/' + trackId,function(data) {
        vm.currentTrack = data;
        vm.showTrackModal = true;
      });
    },
    
    trackEditSubmit() {
        console.log('track edit form submitted');
    },
    
    addTrack() {
      var vm = this;
      var vdata = {
        description: vm.newTrack.description,
        start_time: vm.newTrack.start_time,
        hours: vm.newTrack.hours,
        minutes: vm.newTrack.minutes,
        project_id: vm.currentProject.id
      };
      this.$http.post('track/create',vdata,function(data) {
        vm.showNewUntrackedForm = false;
      	vm.projectDetail(data.project_id);
      });
    },

    deleteTrack(ind,ob) {
      var vm = this;
      // find a better way to confirm
      if(confirm('Are you sure?')) {
        this.$http.delete('track/delete/' + ob.id,function(data) {
          vm.currentProject.projectTracks.splice(ind,1);
        });
      }
      
    }, 

    // reports
    reportSelectAll() {
      this.checkedClients = [];
      for(var cll in this.clients) {
        var n = this.clients[cll].id;
        n = n.toString();
        this.checkedClients.push(n);
      }
    }, 

    reportSelectNone() {
      this.checkedClients = [];
    }, 

    submitReport() {

      for(var a in this.checkedClients) {
        var lookForId = this.checkedClients[a];
            this.$http.get('/test/reports/' + lookForId,function(data) {
              console.log(data);
          });
      }

      var vdata = {
        clients: this.checkedClients,
        start_date: ''
      };

      /*this.$http.post('track/update-description',vdata,function(data) {
          vm.showTrackDescriptionForm = false;
      });*/
        
    }
     

  }
});

