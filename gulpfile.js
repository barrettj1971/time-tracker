var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');
/* 
var bowerDir = './bower_components/';

var lessPaths = [
	bowerDir + 'bootstrap/less',
	bowerDir + 'font-awesome/less'
];
this only needs to be run once. I think there's an easier way 
	to set up something like this 
elixir(function(mix) {
	mix.less('app.less', 'public/css', {paths:lessPaths})				                                   
		.less(['./bower_components/font-awesome/less/font-awesome.less'],'public/css/vendor.css')
		.scripts([
			'jquery/dist/jquery.min.js',
			'bootstrap/dist/js/bootstrap.min.js'
		], 'public/js/vendor.js', bowerDir)
		//.copy('resources/assets/js/app.js','public/js/app.js') i'm not using the resources js file typically 
		.copy(bowerDir + 'font-awesome/fonts', 'public/fonts');
	});*/

/* this is the standard function that will be called frequently */
elixir(function(mix) {
	mix.less('app.less', 'public/css');
	mix.browserify('main.js');
});
